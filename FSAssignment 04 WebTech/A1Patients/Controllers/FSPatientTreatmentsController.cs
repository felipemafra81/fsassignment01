﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using A1Patients.Models;
using Microsoft.AspNetCore.Http;

namespace A1Patients.Controllers
{
    public class FSPatientTreatmentsController : Controller
    {
        private readonly PatientsContext _context;

        public FSPatientTreatmentsController(PatientsContext context)
        {
            _context = context;
        }

        // GET: FSPatientTreatments
        public async Task<IActionResult> Index(int patientDiagnosisId, string patientFullName, string patientDiagnosis)
        {
            if (patientDiagnosisId > 0 )
            {
                HttpContext.Session.SetString(nameof(patientDiagnosisId), patientDiagnosisId.ToString());
                HttpContext.Session.SetString(nameof(patientFullName), patientFullName.ToString());
                HttpContext.Session.SetString(nameof(PatientDiagnosis), patientDiagnosis.ToString());
            }
            else if (HttpContext.Session.GetString(nameof(patientDiagnosisId)) != null)
            {
                patientDiagnosisId = Convert.ToInt32(HttpContext.Session.GetString(nameof(patientDiagnosisId)));
            }
            else
            {
                TempData["message"] = "Please, select a Diagnosis";
                return Redirect("/FSPatientDiagnosis");
            }



            var patientsContext = _context.PatientTreatment.Where(p => p.PatientDiagnosisId == patientDiagnosisId).Include(p => p.PatientDiagnosis).ThenInclude(p => p.Patient).Include(p => p.Treatment).OrderBy(p => p.DatePrescribed); 
            return View(await patientsContext.ToListAsync());
        }

        // GET: FSPatientTreatments/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patientTreatment = await _context.PatientTreatment
                .Include(p => p.PatientDiagnosis)
                .ThenInclude(p => p.Patient)
                .Include(p => p.Treatment)
                .FirstOrDefaultAsync(m => m.PatientTreatmentId == id);
            if (patientTreatment == null)
            {
                return NotFound();
            }

            return View(patientTreatment);
        }

        // GET: FSPatientTreatments/Create
        public IActionResult Create()
        {
            int patientDiagnosisId = Convert.ToInt32(HttpContext.Session.GetString("patientDiagnosisId"));
            var patientDiagnosis = _context.PatientDiagnosis.Find(patientDiagnosisId);


            ViewData["PatientDiagnosisId"] = HttpContext.Session.GetString("patientDiagnosisId");
            ViewData["TreatmentId"] = new SelectList(_context.Treatment.Where(p => p.DiagnosisId == patientDiagnosis.DiagnosisId), "TreatmentId", "Name");
            return View();
        }

        // POST: FSPatientTreatments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PatientTreatmentId,TreatmentId,DatePrescribed,Comments,PatientDiagnosisId")] PatientTreatment patientTreatment)
        {
            if (ModelState.IsValid)
            {
                _context.Add(patientTreatment);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PatientDiagnosisId"] = new SelectList(_context.PatientDiagnosis, "PatientDiagnosisId", "PatientDiagnosisId", patientTreatment.PatientDiagnosisId);
            ViewData["TreatmentId"] = new SelectList(_context.Treatment, "TreatmentId", "Name", patientTreatment.TreatmentId);
            return View(patientTreatment);
        }

        // GET: FSPatientTreatments/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patientTreatment = await _context.PatientTreatment.FindAsync(id);
            if (patientTreatment == null)
            {
                return NotFound();
            }
            ViewData["PatientDiagnosisId"] = new SelectList(_context.PatientDiagnosis, "PatientDiagnosisId", "PatientDiagnosisId", patientTreatment.PatientDiagnosisId);
            ViewData["TreatmentId"] = new SelectList(_context.Treatment, "TreatmentId", "Name", patientTreatment.TreatmentId);
            return View(patientTreatment);
        }

        // POST: FSPatientTreatments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PatientTreatmentId,TreatmentId,DatePrescribed,Comments,PatientDiagnosisId")] PatientTreatment patientTreatment)
        {
            if (id != patientTreatment.PatientTreatmentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(patientTreatment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PatientTreatmentExists(patientTreatment.PatientTreatmentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PatientDiagnosisId"] = new SelectList(_context.PatientDiagnosis, "PatientDiagnosisId", "PatientDiagnosisId", patientTreatment.PatientDiagnosisId);
            ViewData["TreatmentId"] = new SelectList(_context.Treatment, "TreatmentId", "Name", patientTreatment.TreatmentId);
            return View(patientTreatment);
        }

        // GET: FSPatientTreatments/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patientTreatment = await _context.PatientTreatment
                .Include(p => p.PatientDiagnosis)
                .Include(p => p.Treatment)
                .FirstOrDefaultAsync(m => m.PatientTreatmentId == id);
            if (patientTreatment == null)
            {
                return NotFound();
            }

            return View(patientTreatment);
        }

        // POST: FSPatientTreatments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var patientTreatment = await _context.PatientTreatment.FindAsync(id);
            _context.PatientTreatment.Remove(patientTreatment);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PatientTreatmentExists(int id)
        {
            return _context.PatientTreatment.Any(e => e.PatientTreatmentId == id);
        }
    }
}
