﻿using FSClassLibrary;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using A1Patients.Models;

namespace A1Patients.Models
{
    public class FSPatientMetadata
    {
        public int PatientId { get; set; }
        public string FirstName { get; set; }        
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }        
        public string ProvinceCode { get; set; }        
        public string PostalCode { get; set; }
        public string Ohip { get; set; }        
        public DateTime? DateOfBirth { get; set; }
        public bool Deceased { get; set; }      
        public DateTime? DateOfDeath { get; set; }       
        public string HomePhone { get; set; }      
        public string Gender { get; set; }

    }

    [ModelMetadataType(typeof(FSPatientMetadata))]
    public partial class Patient : IValidatableObject
    {
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            PatientsContext _context = new PatientsContext();

            FirstName = FSValidations.FSCapitalize(FirstName);
            LastName = FSValidations.FSCapitalize(LastName);
            Gender = FSValidations.FSCapitalize(Gender);
            FirstName = FSValidations.FSCapitalize(FirstName);
            Address = FSValidations.FSCapitalize(Address);
            City = FSValidations.FSCapitalize(City);

            if (LastName == "" || LastName == null)
            {
                yield return new ValidationResult("Last name required",
                    new[] { nameof(LastName) });
            }

            if (FirstName == "" || LastName == null)
            {
                yield return new ValidationResult("First name required",
                    new[] { nameof(FirstName) });
            }

            bool validProvinceCode = false;

            if (ProvinceCode != null || PostalCode != null)
            {
                ProvinceCode = ProvinceCode.ToUpper();

                if (ProvinceCode == null && PostalCode != null)
                {
                    yield return new ValidationResult("Province Code is required to validate Postal Code",
                    new[] { nameof(PostalCode) });
                }

                if (ProvinceCode != "" && ProvinceCode != null)
                {
                    var result = _context.Province.FirstOrDefault(m => m.ProvinceCode == ProvinceCode);

                    if (result == null)
                    {
                        yield return new ValidationResult("Province Code is not on file",
                        new[] { nameof(ProvinceCode) });
                    }
                    else
                    {
                        validProvinceCode = true;
                    }
                }
            }

            if((PostalCode != null || PostalCode != "") && validProvinceCode == true)
            {
                if (FSValidations.FSPostalCodeProvinceValidation(PostalCode, ProvinceCode) == false)
                {
                    yield return new ValidationResult("Postal code is not according to selected province",
                    new[] { nameof(PostalCode) });
                }  
            }
            else
            {
                yield return new ValidationResult("Province Code is required to validate Postal Code",
                new[] { nameof(PostalCode) });
            }

            if (FSValidations.FSPostalCodeValidation(PostalCode) == false)
            {
                yield return new ValidationResult("Invalid Postal Code",
                new[] { nameof(PostalCode) });
            }

            PostalCode = FSValidations.FSPostalCodeFormat(PostalCode);

            if (Ohip != null && Ohip != "")
            {
                if (!FSValidations.FSValidateOHIP(Ohip))
                {
                    yield return new ValidationResult("OHIP if provided, must match pattern 1234-123-123-XX",
                    new[] { nameof(Ohip) });
                }
            }

            if (HomePhone != null)
            {
                string PatientPhone = FSValidations.FSExtractDigits(HomePhone);

                if (PatientPhone.Length == 10)
                {
                    HomePhone = PatientPhone.Substring(0, 3) + "-" + PatientPhone.Substring(3, 3) + "-" + PatientPhone.Substring(6, 4);
                }
                else
                {
                    yield return new ValidationResult("Home Phone if provided must be 10 digits",
                        new[] { nameof(HomePhone) });
                }
            }            

            string dateDeath = FSValidations.FSValidateDateDeath(DateOfBirth, DateOfDeath, Deceased);

            if (dateDeath != "")
            {
                yield return new ValidationResult(dateDeath,
                    new[] { nameof(DateOfDeath) });
            }


            if (DateOfBirth != null)
            {
                if (FSValidations.FSValidateDateBirth(DateOfBirth) == false){

                    yield return new ValidationResult("Date of Birth can not be in the future",
                    new[] { nameof(DateOfBirth) });
                }
            }

            if (Gender == "" || Gender == null)
            {
                yield return new ValidationResult("Gender required",
                    new[] { nameof(Gender) });
            }
            else if(FSValidations.FSValidateGender(Gender) == false)
            {
                yield return new ValidationResult("Gender must be M, F or X",
                    new[] { nameof(Gender) });
            }



            yield return ValidationResult.Success;

        }
    }
}
