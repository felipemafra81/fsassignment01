﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FSClassLibrary
{
    public static class FSValidations
    {

        /// <summary>
        /// This method converts a string in Capitalized format
        /// Example: felipe santos -> Felipe Santos
        /// </summary>
        /// <param name="a">Any word to be capitalized</param>
        /// <returns>Capitalized String</returns>
        public static string FSCapitalize(string a)
        {
            if (a == null)
            {
                return "";
            }

            string word = a.Trim();
            string[] separatedWord = word.Split();
            string phrase = "";

            foreach (var item in separatedWord)
            {
                string capitalizedWord = "";

                if (item.Length > 1)
                {
                     capitalizedWord = item.First().ToString().ToUpper() + item.Substring(1).ToLower();
                }
                else if(item.Length == 1)
                {
                     capitalizedWord = item.First().ToString().ToUpper();
                }

                if (capitalizedWord != " ")
                {
                    phrase = phrase + " " + capitalizedWord;
                }   

            }

            phrase = phrase.Trim();
            return phrase;
        }

        /// <summary>
        /// This method check if the zipcode is according the province code
        /// Its check if the zipcode first char is compatible with the province
        /// </summary>
        /// <param name="zipCode"></param>
        /// <param name="province"></param>
        /// <returns>True or False Zipcode</returns>
        public static bool FSPostalCodeProvinceValidation(string zipCode, string province)
        {

            bool validZipCode = false;
            string firstChar = zipCode[0].ToString().ToUpper();

            switch (province)
            {

                case "AB":
                    if (firstChar == "T")
                    {
                        validZipCode = true;
                        return validZipCode;
                    }
                    else
                    {
                        return validZipCode;
                    }
                    
                case "BC":
                    if (firstChar == "V")
                    {
                        validZipCode = true;
                        return validZipCode;
                    }
                    else
                    {
                        return validZipCode;
                    }
                    
                case "MB":
                    if (firstChar == "R")
                    {
                        validZipCode = true;
                        return validZipCode;
                    }
                    else
                    {
                        return validZipCode;
                    }
                    
                case "NB":
                    if (firstChar == "E")
                    {
                        validZipCode = true;
                        return validZipCode;
                    }
                    else
                    {
                        return validZipCode;
                    }
                    
                case "NL":
                    if (firstChar == "A")
                    {
                        validZipCode = true;
                        return validZipCode;
                    }
                    else
                    {
                        return validZipCode;
                    }
                    
                case "NS":
                    if (firstChar == "B")
                    {
                        validZipCode = true;
                        return validZipCode;
                    }
                    else
                    {
                        return validZipCode;
                    }
                    
                case "NT":
                    if (firstChar == "X")
                    {
                        validZipCode = true;
                        return validZipCode;
                    }
                    else
                    {
                        return validZipCode;
                    }
                    
                case "NU":
                    if (firstChar == "X")
                    {
                        validZipCode = true;
                        return validZipCode;
                    }
                    else
                    {
                        return validZipCode;
                    }
                    
                case "ON":
                    if (firstChar == "K" || firstChar == "L" || firstChar == "M" ||
                        firstChar == "N" || firstChar == "P")
                    {
                        validZipCode = true;
                        return validZipCode;
                    }
                    else
                    {
                        return validZipCode;
                    }
                    
                case "QC":
                    if (firstChar == "G" || firstChar == "H" || firstChar == "J" ||
                        firstChar == "K")
                    {
                        validZipCode = true;
                        return validZipCode;
                    }
                    else
                    {
                        return validZipCode;
                    }
                    
                case "SK":
                    if (firstChar == "S")
                    {
                        validZipCode = true;
                        return validZipCode;
                    }
                    else
                    {
                        return validZipCode;
                    }
                    
                case "YT":
                    if (firstChar == "Y")
                    {
                        validZipCode = true;
                        return validZipCode;
                    }
                    else
                    {
                        return validZipCode;
                    }
                    
                case "PE":
                    if (firstChar == "C")
                    {
                        validZipCode = true;
                        return validZipCode;
                    }
                    else
                    {
                        return validZipCode;
                    }
                    
                default:
                    return validZipCode;
                    
            }

        }

        /// <summary>
        /// This method validates the zip code according the Canadian pattern
        /// A1A 1A1
        /// </summary>
        /// <param name="zipCode">zipCode</param>
        /// <returns>bool</returns>
        public static bool FSPostalCodeValidation(string zipCode)
        {
            if (zipCode == null || zipCode == "")
            {
                return true;
            }

            string pattern = @"^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ]() ?\d[ABCEGHJKLMNPRSTVWXYZ]\d$";
            
            var validZipCode = true;

            if (!Regex.Match(zipCode, pattern, RegexOptions.IgnoreCase).Success)
            {
                validZipCode = false;
            }

            return validZipCode;
        }

        /// <summary>
        /// This method validates the OHIP according the pattern
        /// 1234-123-123-XX
        /// </summary>
        /// <param name="ohip"></param>
        /// <returns>boll</returns>
        public static bool FSValidateOHIP(string ohip)
        {
            ohip = ohip.ToUpper();

            string pattern = @"[0-9]{4}[-][0-9]{3}[-][0-9]{3}[-][A-Z]{2}$";

            var validOHIP = true;

            if ((!Regex.Match(ohip, pattern).Success))
            {
                validOHIP = false;
            }
            return validOHIP;
        }

        /// <summary>
        /// This method formats the American Zip code according the pattern
        /// </summary>
        /// <param name="zipCode">zipCode</param>
        /// <returns>bool</returns>
        public static bool FSZipCodeValidation(string zipCode)
        {
            string pattern = @"^\d{5}(?:[-\s]\d{4})?$";
            
            var validZipCode = true;

            if ((!Regex.Match(zipCode, pattern).Success))
            {
                validZipCode = false;
            }
            return validZipCode;
        }

        /// <summary>
        /// This method formats the Canadian Zip Code according
        /// the pattern A1A 1A1
        /// </summary>
        /// <param name="postalcode"></param>
        /// <returns></returns>
        public static string FSPostalCodeFormat(string postalcode)
        {
            if (postalcode == null || postalcode == "")
            {
                return postalcode;
            }
            else
            {
                postalcode = postalcode.Replace(" ", "");
                postalcode = postalcode.ToUpper();
                postalcode = postalcode.Substring(0,3) + " " + postalcode.Substring(3,3);
                return postalcode;
            }
        }

        /// <summary>
        /// This method extracts only the phone number digits
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static string FSExtractDigits(string phone)
        {
            string phoneNumbers = new String(phone.Where(Char.IsDigit).ToArray());

            return phoneNumbers;
        }

        /// <summary>
        /// This method checks the nullable DateOfBirth
        /// This Date can not be in the future
        /// </summary>
        /// <param name="birth"></param>
        /// <returns></returns>
        public static bool FSValidateDateBirth(DateTime? birth)
        {
            if (birth > DateTime.Now)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// This method checks the Date of Death
        /// Its compares with the actual date and the birth date to
        /// make sure that the Date of Death is correct.
        /// The meth
        /// </summary>
        /// <param name="birth"></param>
        /// <param name="death"></param>
        /// <param name="deceased"></param>
        /// <returns></returns>
        public static string FSValidateDateDeath(DateTime? birth, DateTime? death, bool deceased)
        {
            if (death == null)
            {          
                    if (deceased)
                    {
                        return "Date of Death must be filled.";
                    }

            }
            else
            {
                if (death > DateTime.Now)
                {
                    return "Date of Death can not be in the future.";
                }

                if (!deceased)
                {
                    return "Please, check deceased.";
                }

                if(birth != null)
                {
                    if (death < birth)
                    {
                        return "Date of Death can not be before the date of birth.";
                    }

                }


            }

            return "";

        }

        /// <summary>
        /// This method validates the gender.
        /// Gender must be M, F or X
        /// </summary>
        /// <param name="genre"></param>
        /// <returns></returns>
        public static bool FSValidateGender(string genre)
        {
            string genderUpper = genre.ToUpper();

            if (genderUpper != "M" && genderUpper != "F" && genderUpper != "X")
            {
                return false;
            }
            else
            {
                return true;
            }
            
        }

    }
}
